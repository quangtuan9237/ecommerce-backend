import Groups, {iUsers} from './model'
import * as _ from 'lodash';
import { Types } from 'mongoose';

export class GroupController {
    static userLists = [];
    static async getById(id: string) {
        if (Types.ObjectId.isValid(id)) {
            let aGroup = await Groups.findById(id);
            if (aGroup) {
                return _.pick(aGroup, ['_id', 'name','users']);
            }
        }
        return false;
    }

    static async getAllGroup(myID: string) {
        if (Types.ObjectId.isValid(myID)) {
            let groups = await Groups.find({"users.userInfo": myID});
            if(!groups) return [];
            return groups;
        }
        return [];
    }

    static async getAllUser(groupID: string) {
        if (Types.ObjectId.isValid(groupID)) {
            let group = await Groups.findById(groupID).populate({ path: 'friends.userInfo', select: 'name _id' });
            if (group) {
                return group.users;
            }
        }
        return [];
    }

    static async createGroup(myID: string, name: string) {
        let user = {
            _id: new Types.ObjectId(myID),
            userInfo: new Types.ObjectId(myID),
            messages: [],
            friendDate: new Date().getTime()
        };

        let group = new Groups();
        group.name = name;
        group.users.push(user);

        await group.save();

        return this.getAllGroup(myID);
    }

    static async removeGroup(group_id: string) {
        let me = await Groups.findByIdAndRemove(group_id);
        return true;
    }

    static async joinGroup(myID:string, group_id: string) {
        let group = await Groups.findById(group_id);

        let user = {
            _id: new Types.ObjectId(myID),
            userInfo: new Types.ObjectId(myID),
            messages: [],
            friendDate: new Date().getTime()
        };

        if(!group.users.id(myID)){
            group.users.push(user);

            await group.save();
        }

        return this.getAllGroup(myID);
    }

    static checkInGroup(myId: string): iUsers {
        return this.userLists.find((friend) => {
            let boolV = friend.userInfo.equals(myId);
            return boolV;
        })
    }
}
