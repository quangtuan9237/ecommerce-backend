import { Schema, model, Document, Types } from 'mongoose'

export interface iGroup extends Document {
  users: any | iUsers,
  name: string,
}

export interface iUsers {
  _id: Types.ObjectId,
  userInfo: Types.ObjectId,
  messages: string[],
  lastDateMessage?: number,
  friendDate: number
}

const user = new Schema({
  userInfo: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  messages: [{
    type: String,
  }],
  lastDateMessage: {
    type: Number,
    default: new Date().getTime()
  },
  friendDate: {
    type: Number,
    default: 0
  },
})

const groupSchema = new Schema({
  users: [user],
  name: {
    type: String,
    require
  }
});

const Groups = model<iGroup>('Groups', groupSchema);

export default Groups;