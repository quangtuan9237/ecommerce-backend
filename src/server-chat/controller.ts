import { iNewMessage, iResponseStatus, iSocketData, iRemoveFriend, iFriend, iUploadFile } from '@/_interface/new_message';
import { ChatServer } from "./server";
import { FriendController } from './friends_model/controller';
import * as WebSocket from 'ws';
import { GroupController } from './groups_model/controller';

export class ServerChatController{
    constructor(private serverChat: ChatServer){
        serverChat.bind("send_message", async (data: iNewMessage) => {
            serverChat.send('send_message_success', data, data.from);

            let group_id = data.receiver;
            let group = await GroupController.getById(group_id);
            if(!group){
                serverChat.send('new_message', data, data.receiver);
            }else{
                group.users.pull(data.from);
                group.users.forEach(user => {
                    data.receiver = user.id;
                    data.from = group_id;
                    serverChat.send('new_message', data, user.id);
                });
            }
        });
    
        serverChat.bind("register_user", async (data: iNewMessage, ws: WebSocket) => {
            let myID = data.from;
            serverChat.registerClient(myID, ws);
            let friends = await FriendController.getAll(myID);
    
            friends.forEach(async(friend)=>{
                serverChat.send('update_status', {friend_id: myID, status: true}, friend.id);
            })
        });
    
        serverChat.bind("close_connection", (data: iNewMessage) => {
            //close the connection
            serverChat.close(data.from);
            console.log(data.from + " is close");
        });
    
        serverChat.bind("find_friend", async (data: iFriend) => {
            let friend = await FriendController.getById(data.friend_id);
            if(!friend){
                serverChat.send('find_friend_result', {}, data.from);
            }else{
                serverChat.send('find_friend_result', friend, data.from);
            }
        });
    
        serverChat.bind("add_friend", async (data: iFriend) => {
            let friends = await FriendController.addFriend(data.from, data.friend_id);
            serverChat.send('friend_list', friends, data.from);
        });
    
        serverChat.bind("remove_friend", async (data: iRemoveFriend) => {
            let friends = await FriendController.removeFriend(data.from, data.friend_id);
            serverChat.send('friend_list', friends, data.from);
        });

        serverChat.bind("get_friends", async (data: iNewMessage) => {
            let friends = await FriendController.getAll(data.from);
            serverChat.send('friend_list', friends, data.from);
        });
    
        serverChat.bind("get_all_status", async (data: iSocketData) => {
            let friends = await FriendController.getAll(data.from);
            friends.forEach(friend => {
                serverChat.send('get_status', data, friend.id);
            });
        });
    
        serverChat.bind("response_status", async (data: iResponseStatus) => {
            serverChat.send('response_status', data, data.receiver);
        });

        serverChat.bind("upload_file", async (data: iUploadFile) => {
            serverChat.send('upload_file', data, data.receiver);
            serverChat.send('upload_file_success', data, data.from);
        });

        serverChat.bind("new_group", async (data: iUploadFile) => {
            let groups = await GroupController.createGroup(data.from, data.name);
            serverChat.send('group_list', groups, data.from);
        });

        serverChat.bind("get_all_groups", async (data: iUploadFile) => {
            let groups = await GroupController.getAllGroup(data.from);
            serverChat.send('group_list', groups, data.from);
        });

        serverChat.bind("remove_group", async (data) => {
            await GroupController.removeGroup(data.group_id);
            let groups = await GroupController.getAllGroup(data.from);
            serverChat.send('group_list', groups, data.from);
        });

        serverChat.bind("find_group", async (data) => {
            let friend = await GroupController.getById(data.group_id);
            serverChat.send('find_group_result', friend, data.from);
        });

        serverChat.bind("join_group", async (data) => {
            let groups = await GroupController.joinGroup(data.from, data.group_id);
            serverChat.send('group_list', groups, data.from);
        });

        serverChat.bindOnClose(async (evt: WebSocket.CloseEvent) => {
                let friends = await FriendController.getAll(evt.target['id']);
        
                friends.forEach(friend => {
                    let data: iResponseStatus = {
                        from: evt.target['id'],
                        receiver: friend.id,
                        status: false,
                        time_date: new Date().getTime(),
                    }
        
                    this.serverChat.send('response_status', data, friend.id);
                });
            }
        )
    }
}