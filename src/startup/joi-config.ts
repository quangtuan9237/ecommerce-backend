import * as Joi from 'joi'
import joiObjectid from 'joi-objectid'

export function joiConfigInit() {
  Joi["objectId"] = joiObjectid(Joi);
}

