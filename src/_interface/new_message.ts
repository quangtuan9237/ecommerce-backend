export interface iSocket {
    event: string,
    data: iSocketData,
    token: string,
}

export interface iSocketData {
    from: string,
    time_date: number,
}

export interface iRemoveFriend extends iSocketData {
    friend_id: string,
}

export interface iNewMessage extends iSocketData {
    receiver: string,
    message: string,
}

export interface iResponseStatus extends iSocketData {
    receiver: string,
    status: boolean
}

export interface iFriend extends iSocketData {
    friend_id: string,
}

export interface iUploadFile extends iSocketData {
    receiver: string,
    name: string,
    type: string,
    size: string,
    data: string,
}