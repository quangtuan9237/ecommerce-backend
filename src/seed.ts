import User from './api/users/model';
import * as config from 'config'

const adminInfo ={
    name: config.get('ecommerce_admin_name'),
    email: config.get('ecommerce_admin_email'),
    password: config.get('ecommerce_admin_pass'),
    isAdmin: true
}

export async function createAdmin() {
    let user = await User.findOne({ email: adminInfo.email });
    if(user){
        console.log('User already registered.');
    }else{
        user = new User(adminInfo);
        await user.hashPassword();
        await user.save();
    }
}

